import pytest
from datetime import datetime, date, time
from normalizer import fix_timestamp, fix_zipcode, fix_duration, fix_fullname
from pytz import timezone


@pytest.fixture
def timestamp():
    return '4/1/11 11:00:00 AM'

@pytest.fixture
def short_zipcode():
    return '4'

@pytest.fixture
def duration():
    return '1:23:45.67'

@pytest.fixture
def expected_duration():
    return 5025.67

@pytest.fixture
def english_fullname():
    return 'John Smith'

@pytest.fixture
def unicode_fullname():
    return 'Jöhñ Smìth'

def test_timestamp_is_eastern(timestamp):
    assert 'US/Eastern' == fix_timestamp(timestamp).tzinfo.zone

def test_timestamp_is_correct_time(timestamp):
    expected = time(14, 0, tzinfo=timezone('US/Eastern'))
    assert expected == fix_timestamp(timestamp).time()

def test_short_zipcode(short_zipcode):
    assert 5 == len(fix_zipcode(short_zipcode))

def test_zipcode_padded(short_zipcode):
    expected = '00004'
    assert expected in fix_zipcode(short_zipcode)

def test_converted_to_seconds(duration, expected_duration):
    assert expected_duration == fix_duration(duration)

def test_full_name_english_upper(english_fullname):
    assert english_fullname.upper() in fix_fullname(english_fullname)

def test_full_name_unicode(unicode_fullname):
    assert unicode_fullname.upper() in fix_fullname(unicode_fullname)