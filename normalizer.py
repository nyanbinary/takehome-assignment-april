"""
## Full Description

Please write a tool that reads a CSV formatted file on `stdin` and
emits a normalized CSV formatted file on `stdout`. For example, if
your program was named `normalizer` we would test your code on the
command line like this:

```sh
./normalizer < sample.csv > output.csv
```

Normalized, in this case, means:

* The entire CSV is in the UTF-8 character set.
* The `Timestamp` column should be formatted in RFC3339 format.✔️
* The `Timestamp` column should be assumed to be in US/Pacific time;
  please convert it to US/Eastern. ✔️ 
* All `ZIP` codes should be formatted as 5 digits. If there are less
  than 5 digits, assume 0 as the prefix. ✔️ 
* The `FullName` column should be converted to uppercase. There will be
  non-English names.✔️
* The `Address` column should be passed through as is, except for
  Unicode validation. Please note there are commas in the Address
  field; your CSV parsing will need to take that into account. Commas
  will only be present inside a quoted string. ✔️
* The `FooDuration` and `BarDuration` columns are in HH:MM:SS.MS
  format (where MS is milliseconds); please convert them to the
  total number of seconds. ✔️
* The `TotalDuration` column is filled with garbage data. For each
  row, please replace the value of `TotalDuration` with the sum of
  `FooDuration` and `BarDuration`. ✔️
* The `Notes` column is free form text input by end-users; please do
  not perform any transformations on this column. If there are invalid
  UTF-8 characters, please replace them with the Unicode Replacement
  Character.

Safe Assumptions:

* The input document is in UTF-8.
* Invalid characters can be replaced with the Unicode Replacement Character. If that replacement makes data invalid (for example, because it turns a date field into something unparseable), print a warning to `stderr` and drop the row from your output.
* Times that are missing timezone information are in `US/Pacific`.
* The sample data we provide contains all date and time format variants you will need to handle.
* Any type of line endings are permissible in the output.
"""

import csv
import sys 
from datetime import datetime
import time
from dateutil import tz, parser
from pytz import timezone
import pytz
import ftfy

#constants for conversion
HOUR = 3600
MINUTE = 60

def fix_timestamp(timestamp):
  eastern = timezone("US/Eastern")
  parsed = parser.parse(timestamp, default=datetime(2021, 1, 1, tzinfo=tz.gettz("US/Pacific")))
  fixed = parsed.astimezone(eastern)
  return fixed

def fix_zipcode(zipcode):
  if len(zipcode) < 5:
    zipcode = f"{zipcode:0>5}"
  return zipcode

def fix_fullname(fullname):
  return fullname.upper()

def fix_duration(duration):
  tokens = [float(x) for x in duration.split(":")]
  return (tokens[0] * HOUR) + (tokens[1] * MINUTE) + tokens[2]

def fix_notes(notes):
  return ftfy.fix_text(notes)

if __name__ == "__main__":
  in_file = sys.argv[1]
  out_file = sys.argv[2]
  with open(in_file, 'r', encoding='utf-8') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',')
    fieldnames = reader.fieldnames
    fixed = []
    for row in reader:
      row['Timestamp'] = fix_timestamp(row['Timestamp'])
      row['ZIP'] = fix_zipcode(row['ZIP'])
      row['FullName'] = fix_fullname(row['FullName'])
      row['FooDuration'] = fix_duration(row['FooDuration'])
      row['BarDuration'] = fix_duration(row['BarDuration'])
      row['TotalDuration'] = row['FooDuration'] + row['BarDuration']
      row['Notes'] = fix_notes(row['Notes'])
      fixed.append(row)

  with open(out_file, 'w', encoding='utf-8') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerows(fixed)

