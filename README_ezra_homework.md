## Assumptions
This guide assumes `python` is `python3`. This was written on a system running python 3.9.

## Set up
1. install the requirements with `pip` or `pipenv`. 

## Running the normalizer
The normalizer takes a path to a csv file, and a path to an output file, if the output file does not exist, it will make one.  
The syntax for running it is as follows:
> `python normalizer.py <infile> <outfile>`

## Running tests
>`pytest -v tests.py`
